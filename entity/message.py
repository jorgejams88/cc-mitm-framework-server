import json
import uuid

KEYS = ["origin", "type", "uuid", "apduRequest", "apduResponse"]
MESSAGE_ORIGIN = {"CARD": 0, "READER": 1}
MESSAGE_TYPE = {"AUTH": 0, "TX": 1, "NOTIFICATION": 3}


class Message:
    def __init__(self, origin=None, type=None, uuid=None, apduRequest=None, apduResponse=None):
        self.origin = origin
        self.type = type
        self.uuid = uuid
        self.apduRequest = apduRequest
        self.apduResponse = apduResponse

    def generateUUID(self):
        print("generating uuid")
        self.uuid = uuid.uuid4().__str__()


def messageFromJson(jsonString):
    obj = json.loads(jsonString)
    ret = Message()
    for k in KEYS:
        if k in obj:
            setattr(ret, k, obj[k])
    return ret


def jsonFromMessage(message):
    tmpObj = {}
    for k in KEYS:
        if hasattr(message, k):
            attr = getattr(message, k)
            if (attr != None):
                tmpObj[k] = getattr(message, k)
    return json.dumps(tmpObj)
