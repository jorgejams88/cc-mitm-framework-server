import tornado.web
import tornado.websocket
from entity.message import *


class WSHandler(tornado.websocket.WebSocketHandler):

    def open(self):
        self.connection_open = True
        print('New connection')

    def on_message(self, message):

        messageObj = messageFromJson(message)
        if messageObj.type == MESSAGE_TYPE["AUTH"]:
            print('Auth message received:  %s' % message)
            if messageObj.origin == MESSAGE_ORIGIN["CARD"]:
                if WSHandler.card != None:
                    errorMessage = "There is an ongoing transaction, please finish it first"
                    print(errorMessage)
                    self.close(reason=errorMessage)
                else:
                    if WSHandler.reader == None:
                        errorMessage = "The reader must connect before the card"
                        print(errorMessage)
                        self.close(reason=errorMessage)
                    else:
                        WSHandler.card = self
                        messageObj.uuid = WSHandler.transactionId
                        self.write_message(jsonFromMessage(messageObj))
                        print(jsonFromMessage(messageObj))
                        print("The transaction can begin")
                        notification = Message()
                        notification.uuid = WSHandler.transactionId
                        notification.type = MESSAGE_TYPE["NOTIFICATION"]
                        notification.apduRequest = "<center><font color='green'><b>The attack can begin</b></font></center>"
                        WSHandler.reader.write_message(jsonFromMessage(notification))
            elif messageObj.origin == MESSAGE_ORIGIN["READER"]:
                if WSHandler.reader != None:
                    errorMessage = "There is an ongoing transaction, please finish it first"
                    print(errorMessage)
                    self.close(reason=errorMessage)
                else:
                    messageObj.generateUUID()
                    WSHandler.transactionId = messageObj.uuid
                    WSHandler.reader = self
                    self.write_message(jsonFromMessage(messageObj))
                    print(jsonFromMessage(messageObj))
                    print("Reader is connected, waiting for card to connect")
        elif messageObj.type == MESSAGE_TYPE["TX"]:
            if messageObj.origin == MESSAGE_ORIGIN["READER"]:
                print('Message from reader received:  %s' % message)
                WSHandler.card.write_message(jsonFromMessage(messageObj))
            elif messageObj.origin == MESSAGE_ORIGIN["CARD"]:
                print('Message from card received:  %s' % message)
                WSHandler.reader.write_message(jsonFromMessage(messageObj))

    def on_close(self):
        self.connection_open = False
        print('connection closed on_close')
        if (WSHandler.card == self):
            WSHandler.card = None
        if (WSHandler.reader == self):
            WSHandler.reader = None
        # self.loop.stop()

    def on_connection_close(self):
        self.connection_open = False
        print('connection closed on_connection_close')
        self.handle_disconnection()

    def check_origin(self, origin):
        return True

    def on_finish(self):
        print('on_finish')
        if (WSHandler.card == self):
            WSHandler.card = None
        if (WSHandler.reader == self):
            WSHandler.reader = None

    def handle_disconnection(self):
        WSHandler.transactionId = None
        if (WSHandler.card == self):
            WSHandler.card = None
            if (WSHandler.reader != None):
                WSHandler.reader.close(reason="The card has disconnected")
                WSHandler.reader = None

        if (WSHandler.reader == self):
            WSHandler.reader = None
            if (WSHandler.card != None):
                WSHandler.card.close(reason="The reader has disconnected")
                WSHandler.card = None
