import tornado.ioloop
import tornado.web
from socket_server import WSHandler

if __name__ == "__main__":
    application = tornado.web.Application([
        (r'/ws', WSHandler),
    ])
    WSHandler.card = None
    WSHandler.reader = None
    WSHandler.transactionId = None
    http_server = tornado.httpserver.HTTPServer(application)
    http_server.listen(8888)
    print('*** Websocket Server Started ***')
    tornado.ioloop.IOLoop.instance().start()
